using VMEC
using BenchmarkTools
using StellaratorOptimizationMetrics
using ProfileVega
#vmec = VMEC.readVmecWout("wout_wista_15_beta_197.nc")
vmec = VMEC.readVmecWout("wout_wista_15_beta_0.nc")

#surfs = collect(0.01:0.01:0.99)
surfs = [0.5]

nwells = 10

#θs = [0.0, π/8, π/4, 3*π/8, π/2]
#ζs = [0.0, π/2, π, 3*π/2, 2*π]
#θs = [π/4]
#ζs = [1.0*π]
θs = [0.0]
ζs = [0.0]

StellOptMetrics.BallooningStability.cobra(vmec, surfs, nwells, θs = θs, ζs = ζs)

function f_prof(N)
    for ii = 1:N
        cobravmec.cobra(vmec, surfs, nwells, θs = θs, ζs = ζs)
    end
end

@profview f_prof
