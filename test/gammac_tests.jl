@testset "GammaC tests" begin
  rtol_lo = 1.0E-12
  rtol = 1.0E-4
  allExtrema = StellaratorOptimizationMetrics.GammaC.allExtrema
  calculateWells = StellaratorOptimizationMetrics.GammaC.calculateWells
  ConstructBTargetRange = StellaratorOptimizationMetrics.GammaC.ConstructBTargetRange
  @testset "Find extrema and roots test" begin
    #use sin(x)/x to test for extrema
    #these have extrema when tan(x) = x
    n = 30
    ζ = 0.1:0.01:n*π
    sinc_v = [sin(x)/x for x in ζ]#note we don't use julia sinc b/c of pi normalization
    sinc_spline = CubicSplineInterpolation(ζ, sinc_v)
    ζextreme = allExtrema(sinc_spline,
                  first(ζ), last(ζ), step(ζ)/4)
    #construct the roots
    f(x) = tan(x) - x
    
    ζroots = [find_zero(f, (π*i/2+100*eps(), π*(i+2)/2-100*eps())) for i in 1:2:(2*n)-2]
    @test isapprox(ζextreme, ζroots, rtol=rtol)
    #get wells
    wells = calculateWells(ζextreme, 0.0, sinc_spline)
    #wells appear between (3π, 4π), (5π, 6π) etc
    last_index = findfirst(x->x==wells[end][3], ζextreme)
    well_construct = [(i*π, (i+1)*π, ζroots[i]) for i in 3:2:last_index]
    #for some reason I can't check the whole array at once, so we do term by term
    for i in 1:length(wells)
      for j in 1:3
        @test isapprox(wells[i][j], well_construct[i][j], rtol=rtol)
      end
    end
    @testset "test ConstructBTargetRange" begin
      bres = 100
      #note there's a hidden tolerance in the function that makes things off by 1.0E-6
      (minb, brange) = ConstructBTargetRange(sinc_v, bres)
      @test minb == minimum(sinc_v)
      maxb = maximum(sinc_v)
      @test isapprox(brange[end], maxb-brange.step.hi, rtol=rtol*10)
      @test brange.len == bres
      @test isapprox(minb + brange.step.hi, brange[1], rtol=rtol*10)
    end
  end

  vmecfile = joinpath(@__DIR__,"wout_ku4.nc")
  vmec = VMEC.readVmecWout(vmecfile)
  vmecsurf = VmecSurface(0.5, vmec)
  ζRange = 0:2*π/100:200*π
  setup_gamma_c_splines = StellaratorOptimizationMetrics.GammaC.setup_gamma_c_splines
  B, Bsupζ, dBdψ, ∇ψ, e_θ, κg, VT, Bmag = setup_gamma_c_splines(vmecsurf, ζRange);
  T = typeof(B)
  L = length(B)
  @testset "GammaC splines test" begin
    @test typeof(Bsupζ) == T
    @test typeof(dBdψ) == T
    @test typeof(∇ψ) == T
    @test typeof(e_θ) == T
    @test typeof(κg) == T
    @test typeof(VT) == T
    @test length(Bsupζ) == L
    @test length(dBdψ) == L
    @test length(∇ψ) == L
    @test length(e_θ) == L
    @test length(κg) == L
    @test length(VT) == L
    @test length(Bmag) == L
  end
  ζExtreme = allExtrema(B,first(ζRange),last(ζRange),step(ζRange)/4)
  bprime = 1.2 #consider only one value
  wellBounds = calculateWells(ζExtreme, bprime, B)
  (Bmin, BTargetRange) = ConstructBTargetRange(Bmag, 100)

  @testset "test Integrand functions" begin
    #extract a single well
    dIdBIntegrand = StellaratorOptimizationMetrics.GammaC.dIdBIntegrand
    dgdBIntegrand = StellaratorOptimizationMetrics.GammaC.dgdBIntegrand
    dGdBIntegrand = StellaratorOptimizationMetrics.GammaC.dGdBIntegrand
    dVdBIntegrand = StellaratorOptimizationMetrics.GammaC.dVdBIntegrand
    IIntegrand = StellaratorOptimizationMetrics.GammaC.IIntegrand
    HIntegrand = StellaratorOptimizationMetrics.GammaC.HIntegrand
    lbound = wellBounds[1][1]+1e5*eps()
    rbound = wellBounds[1][2]-1e5*eps()
    #the values from the integration are from a version benchmarked against ROSE
    dIdBValue = quadde(x->dIdBIntegrand(x,B,Bsupζ,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(dIdBValue, 7.345150279098044, rtol=rtol)
    dgdBValue = quadde(x->dgdBIntegrand(x,B,Bsupζ,∇ψ,κg,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(dgdBValue, -0.04129151235196734, rtol=rtol)
    dGdBValue = quadde(x->dGdBIntegrand(x,B,Bsupζ,dBdψ,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(dGdBValue, 0.14212742651969562, rtol=rtol)
    dVdBValue = quadde(x->dVdBIntegrand(x,B,Bsupζ,VT,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(dVdBValue, -0.5779273317734693, rtol=rtol)
    IValue = quadde(x->IIntegrand(x,B,Bsupζ,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(IValue, 2.884393249859751, rtol=rtol)
    HValue = quadde(x->HIntegrand(x,B,Bsupζ,∇ψ,κg,Bmin,bprime),lbound,rbound)[1]
    @test isapprox(HValue, -0.03509574106924143, rtol=rtol)
  end
  @testset "test full integration" begin
    a = gamma_c_target(vmecsurf, 0.0, 2*π/100, 200*π, 200) 
    @test isapprox(a[1], 0.0010316761236487534, rtol=rtol)
    @test isapprox(a[2], 0.008381663079191611, rtol=rtol)
  end
end
    
