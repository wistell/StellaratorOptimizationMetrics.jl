 struct RatFourier
     num::AbstractVector
     den::AbstractVector

     function RatFourier(N::Integer)
         num = zeros(2N+1);
         den = zeros(2N+1);
         den[1] = 1.

         return new(num, den)
     end
 end

function get_num(r::RatFourier); return r.num; end
function get_den(r::RatFourier); return r.den; end
function get_N(r::RatFourier); return length(r.num)÷2; end

function set_num!(r::RatFourier, num::AbstractArray)
    r.num[:] = num[:];
end

function set_den!(r::RatFourier, den::AbstractArray)
    r.den[2:end] = den[:];
end

function FourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = cos.(θ * (0:Nθ)');
  A[:, 2:2:end] = sin.(θ * (1:Nθ)');

  return A
end

function dFourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = - sin.(θ * (0:Nθ)')*Diagonal(0:Nθ);
  A[:, 2:2:end] = cos.(θ * (1:Nθ)')*Diagonal(1:Nθ);

  return A
end

function ddFourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = - cos.(θ * (0:Nθ)')*Diagonal((0:Nθ).^2)
  A[:, 2:2:end] = - sin.(θ * (1:Nθ)')*Diagonal((1:Nθ).^2)

  return A
end

function eval(r::RatFourier, θ::AbstractVector; A::AbstractArray=[])
  A = (A == []) ? FourierMat(θ, get_N(r)) : A;
  n = A*get_num(r);
  d = A*get_den(r);
  @assert all(>(0.), d)

  return n./d
end

function eval(r::RatFourier, θ::Number; A::AbstractArray=[])
  return eval(r, [θ])[1]
end

function deval(r::RatFourier, θ::AbstractVector; A::AbstractArray=[], dA::AbstractArray=[])
  N = get_N(r); num = get_num(r); den = get_den(r);

  A = (A==[]) ? FourierMat(θ, N) : A;
  dA = (dA==[]) ? dFourierMat(θ, N) : dA;
  n = A*num
  dn = dA*num
  d = A*den
  dd = dA*den

  @assert all(>(0.), d)

  return dn./d - dd.*n ./ (d.^2)
end

function deval(r::RatFourier, θ::Number; A::AbstractArray=[], dA::AbstractArray=[])
  return deval(r, [θ]; A, dA)[1]
end

function ddeval(r::RatFourier, θ::AbstractVector; A::AbstractArray=[],
                dA::AbstractArray=[], ddA::AbstractArray=[])
  N = get_N(r); num = get_num(r); den = get_den(r);


  A = (A==[]) ? FourierMat(θ, N) : A;
  dA = (dA==[]) ? dFourierMat(θ, N) : dA;
  ddA = (ddA==[]) ? ddFourierMat(θ, N) : ddA;
  n = A*num
  dn = dA*num
  ddn = ddA*num
  d = A*den
  dd = dA*den
  ddd = ddA*den

  @assert all(>(0.), d)

  return(  2 .*n.*dd.*dd./(d.^3)
         - (2 .*dd.*dn + n.*ddd) ./ (d.^2)
         + ddn ./ d )
end

function ddeval(r::RatFourier, θ::Number; A::AbstractArray=[],
                dA::AbstractArray=[], ddA::AbstractArray=[])
  return ddeval(r, [θ]; A, dA, ddA)[1]
end

# Linearized least squares approximation to the function
#   Note: approximation error is different than the nonlinear fit
function lls_RatFourier(θ::AbstractVector, z::AbstractVector, N::Integer)
  AF = FourierMat(θ, N);
  Nθ = length(θ)

  A = zeros(Nθ, 4N+1)
  A[:, 1:2N+1] = AF;
  A[:, 2N+2:end] = -Diagonal(z) * AF[:, 2:end];

  b = z;

  x = A\b;

  r = RatFourier(N);
  set_num!(r, x[1:2N+1]);
  set_den!(r, x[2N+2:end])

  return r;
end

function lls_Fourier(θ::AbstractVector, z::AbstractVector, N::Integer)
  A = FourierMat(θ, 2N);
  b = z;
  coef = A\b;

  return coef;
end

function interp_RatFourier(θ::AbstractVector, z::AbstractVector)
  N = length(θ)÷4;
  @assert length(θ) == 4N+1

  return lls_RatFourier(θ, z, N)
end

function resid_RatFourier!(resid, r, θ, z, AF)
  resid[:] = eval(r, θ; A=AF) - z;
end

function jacob_RatFourier(J, r, θ, z, AF)
  N = get_N(r)
  n = AF * get_num(r);
  d = AF * get_den(r);

  J[:, 1:2N+1] = Diagonal(1 ./ d) * AF;
  J[:, 2N+2:end] = -Diagonal(n ./ d.^2) * AF[:, 2:end];
end

function hess_RatFourier(H, resid, J, r, θ, z, AF);
  N = get_N(r)
  H[:, :] = J'*J

  den = get_den(r);
  num = get_num(r);
  for ii = 1:length(θ)
    vi = AF[ii, :];
    vid = vi[2:end];
    residi = resid[ii];

    d = vi'*den;
    n = vi'*num;
    H[1:2N+1, 2N+2:end] += (-residi/d^2) .* vi*vid'
    H[2N+2:end, 1:2N+1] += (-residi/d^2) .* vid*vi'
    H[2N+2:end, 2N+2:end] += (2n*residi/d^3) .* vid*vid'
  end
end

function nls_RatFourier(θ::AbstractVector, z::AbstractVector, N::Integer)
  r = lls_RatFourier(θ, z, N)
  Nθ = length(θ)

  resid = zeros(Nθ);
  J = zeros(Nθ, 4N+1);
  H = zeros(4N+1, 4N+1)
  AF = FourierMat(θ, N);

  resid_RatFourier!(resid, r, θ, z, AF)
  jacob_RatFourier(J, r, θ, z, AF)
  hess_RatFourier(H, resid, J, r, θ, z, AF)

  iimax = 1;
  for ii = 1:iimax
    # println("ii=$ii, err = $(norm(resid)) ")
    # Δcoef = - (J'*J) \ (J'*resid);
    Δcoef = - (J \ resid);
    # Δcoef = - H \ (J'*resid)
    set_num!(r, get_num(r) + Δcoef[1:2N+1])
    set_den!(r, get_den(r)[2:end] + Δcoef[2N+2:end])

    resid_RatFourier!(resid, r, θ, z, AF)
    if ii != iimax
      jacob_RatFourier(J, r, θ, z, AF)
      hess_RatFourier(H, resid, J, r, θ, z, AF)
    end
  end
  # println("exiting with err = $(norm(resid)) ")

  return r
end
