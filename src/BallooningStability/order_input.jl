#This is the old version and is deprecated, but kept around in case there's a reason to keep it
"""
function order_input(vmec_data::VmecData)
   #= if module
   export nfp_v, ns_cob, mnmax_v, lasym_v, hiota, hpres, hphip, r0, amin, beta0, b0_v,
          xm_v, xn_v, mndim, rmncf, zmnsf, bmnch, lmnsh, bsupvmnch, bsupumnch, list, nlist,
          rmnsf, zmncf, bmnsh, lmnch, bsupvmnsh, bsupumnsh
   =#
   mu0 = 4π*1e-7;

   #= Include if error handling is to be added
   IF (ierr_vmec.ne.0 .or. ierr.ne.0) GOTO 1000x
   =#

   ns = vmec_data.ns;
   aspect_v = vmec_data.aspect;	# local
   r0max_v = vmec_data.rmax_surf;	# local
   r0min_v = vmec_data.rmin_surf;	# local
   r0 = (r0max_v + r0min_v)/2; #return
   betaxis_v = vmec_data.betaxis;	# local

   #...   IOTA, PRES, PHIP are ALL on HALF-MESH###

   hpres = vmec_data.pres;
   hpres = mu0*hpres;   # Units for pressure in > v6.0 are Pa. The code uses T^2


   amin = r0/aspect_v; #return
   beta0 = betaxis_v;   #return
   if beta0 <= 0.0      # adjust to infinitesimally small profile
      beta0 = eps(Float64);
      hpres = beta0*([ns-1:-1:0;]/(ns-1));
      #println(hpres[2]," ",hpres[3])
   end
   b0_v = sqrt(2*(1.5*hpres[2] - 0.5*hpres[3])/beta0); # export


   return (r0, amin, beta0, b0_v)

end
"""
function order_input(surf::S) where {S<:AbstractSurface}
  return surf.Rmajor_p, surf.Aminor_p, surf.betaxis, surf.b0_v
end  
