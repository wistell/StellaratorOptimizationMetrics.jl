module BallooningStability

export
    cobra,
    cobra_opt

using Printf
using NetCDF: ncread
using PlasmaEquilibriumToolkit
using LinearAlgebra
using SparseArrays
using ArnoldiMethod
using Optim
using Interpolations

# to throw errors and stop the program
struct StopException{T}
    S::T
end
stop(error_message="Stopped.") = throw(StopException(error_message));

function Base.showerror(io::IO, ex::StopException, bt; backtrace=true)
    showerror(io, ex.S)
end

include("cobra.jl")
include("order_input.jl")
include("get_ballooning_grate.jl")
include("getmatrix.jl")
include("coeffs.jl")
include("summodosd.jl")
include("BalloonCoefInterp.jl")
include("eig_opt.jl")

end

using .BallooningStability
