## Find the maximal eigenvalue in a domain (α, ζ) ∈ D = [0,2π) × [0,2π/nfp)
# Formally, the problem is
#   min {λ | λ ≥ (xᵀ K(α,ζ) x)/(xᵀ M(α, ζ) x) ∀ x ∈ ℝⁿ, (α, ζ) ∈ D}
# When framed this way, we can use the fact that the eigenvalues can be
# written as a family of n differentiable functions of α and ζ. This gives
# the interpretation as λ being an optimization a piecewise continuous function.


# Multiplies (KM + MK)/2, where K is SymTridiagonal and M is Diagonal
function symmult_ev(ev, M)
    return 0.5 .* (ev .* M[1:end-1] + ev .* M[2:end]);
end

function conjugate(K::SymTridiagonal, M::Diagonal)
    Minv = 1 ./ diag(M)
    Minv12 = sqrt.(Minv); # M inverse to the 1/2

    dv = Minv .* diag(K,0);
    ev = Minv12[1:end-1] .* Minv12[2:end] .* diag(K,1);

    return SymTridiagonal(dv, ev);
end

function get_λx(K, M)
    Minv = 1 ./ diag(M)
    Minv12 = sqrt.(Minv); # M inverse to the 1/2

    dv = Minv .* diag(K,0);
    ev = Minv12[1:end-1] .* Minv12[2:end] .* diag(K,1);

    Kconj = SymTridiagonal(dv, ev);

    n = size(Kconj, 1);
    λ, y = LinearAlgebra.LAPACK.stegr!('V', 'I', Kconj.dv, Kconj.ev, 0, 0, n, n)
    x = Minv12 .* y[:]
    return λ[1], x
end

function λx_update!(x, K, M, λ0)
    # Rayleigh iteration
    dλ = 1;
    λnm1 = λ0
    λn = 0;

    x[:] = x ./ (x'*M*x);

    for ii = 1:10
        λn = (x'*K*x);
        dλ = λn - λnm1
        λnm1 = λn

        x[:] = (K-λn.*M)\x;
        x[:] = x ./ (x'*M*x);

        if abs(dλ) < 1e-6
            return λn
        end
    end

    println("λx_update! did not converge")
    return λn
end

function get_λx_derivs(K, M, dK, dM, ddK, ddM, λ, x)
    N = length(x);
    Mx = M*x;
    lhs = vcat(hcat(sparse(K) - λ .* M, -Mx), hcat(-Mx', [0]));
    rhs = zeros(N+1, 2);
    for ii = 1:2
        dKii = dK[ii];
        dMii = dM[ii];
        rhs[1:end-1, ii] = -(dKii - λ.*dMii)*x;
        rhs[end, ii] = 0.5 * x' * dMii * x;
    end

    ans = lhs \ rhs;
    dλ = ans[end, :];
    dx = ans[1:end-1, :];

    rhs = zeros(N+1, 2, 2);
    for ii = 1:2, jj = 1:2
        ddresid = ddK[ii, jj] - dλ[ii] .* dM[jj] - dλ[jj] .* dM[ii] - λ .* ddM[ii,jj];
        dresid_ii = dK[ii] - λ .* dM[ii] - dλ[ii] .* M;
        dresid_jj = dK[jj] - λ .* dM[jj] - dλ[jj] .* M;
        rhs[1:end-1, ii, jj] = -(ddresid*x + dresid_ii*dx[:,jj] + dresid_jj*dx[:,ii]);

        rhs[end, ii, jj] = (0.5 * x' * ddM[ii, jj] * x +
                                  x' * dM[ii] * dx[:, jj] +
                                  x' * dM[jj] * dx[:, ii] +
                                  dx[:, ii]' * M * dx[:, jj])
    end

    ans = lhs \ reshape(rhs, N+1, 4)
    ddx = reshape(ans[1:end-1, :], N, 2, 2)
    ddλ = reshape(ans[end, :], 2, 2)

    return dλ, dx, ddλ, ddx
end

function get_fgh!(KMfun, nfp)
    function fgh!(F, G, H, r)
        r = copy(r);
        r[1] = mod(r[1], 2π);
        r[2] = mod(r[2], 2π/nfp);
        K, dK, ddK, M, dM, ddM = KMfun(r);
        λ, x = get_λx(K, M)
        dλ, dx, ddλ, ddx = get_λx_derivs(K, M, dK, dM, ddK, ddM, λ, x)

        !(G == nothing) && (G[:] = -dλ);
        !(H == nothing) && (H[:,:] = -ddλ);

        if !(F == nothing)
            return -λ
        end
    end

    return fgh!
end

# Kfun: (θ, ζ) → (K, dK, ddK) (K assumed SymTridiagonal, dK 2-vector derivatives, ddK 2x2 matrix of second derivatives)
# Mfun: (θ, ζ) → (M, dM, ddM) (M assumed Diagonal)
# r0  : Initial point (θ, ζ)
function eig_opt(Kfun::Function, Mfun::Function, r0::AbstractVector, nfp::Integer)
    KMfun = (r) -> begin
        K, dK, ddK = Kfun(r);
        M, dM, ddM = Mfun(r);
        return K, dK, ddK, M, dM, ddM
    end

    return eig_opt(KMfun, r0, nfp);
end

function eig_opt(KMfun::Function, r0::AbstractVector, nfp::Integer)
    # Optimize
    fgh! = get_fgh!(KMfun, nfp);
    res = Optim.optimize(Optim.only_fgh!(fgh!), r0, Optim.NewtonTrustRegion())

    r = res.minimizer;
    r[1] = mod(r[1], 2π);
    r[2] = mod(r[2], 2π/nfp);

    return res.minimizer, -res.minimum
end
