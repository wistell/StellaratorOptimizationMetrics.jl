function getmatrix(surf::S, s::Float64, init_zeta::Float64, init_theta::Float64,
                   xmin::Float64, h::Float64, n::Int64,
                   coeffs_f::coeffs_struct, coeffs_h::coeffs_struct,
                  params::BalloonParams, bci::BallooningCoefInterp, interp::Bool
                  ) where {S <: AbstractSurface}

  coeffs_f.x .= xmin .+ [0:n-1;]*h;                    # build full mesh,
  lfail_balloon=false;
  init_alpha = init_theta - surf.iota[1]*init_zeta;

  init_internal_theta = 0.;
  init_internal_zeta = 0.;

  if !interp
     ψpest = -surf.phi[1]/2/π
     pestCoords = PestCoordinates(ψpest, init_alpha, -init_zeta)
     internal_coords = InternalFromPest()(pestCoords, surf)
     init_internal_theta = internal_coords.θ
     init_internal_zeta = internal_coords.ζ
  end

  if interp
    lfail_balloon = coeffs(bci, init_zeta, init_alpha, coeffs_f; eval_p=false)
  else
    lfail_balloon = coeffs(surf, s, init_internal_zeta, init_internal_theta, n, coeffs_f, params);          # evaluate coefficients on full mesh
  end
  if lfail_balloon
     return lfail_balloon
  end


  coeffs_h.x .= coeffs_f.x[2:n] .- h/2;                              # build half mesh,
  if interp
    lfail_balloon = coeffs(bci, init_zeta, init_alpha, coeffs_h; eval_q=false, eval_r=false)
  else
    lfail_balloon = coeffs(surf, s, init_internal_zeta, init_internal_theta, n-1, coeffs_h, params);           # evaluate coefficients on half mesh
  end
  if lfail_balloon
     return lfail_balloon
  end

  p = coeffs_h.p;
  q = coeffs_f.q;
  r = coeffs_f.r;

  dv = (-p[2:n-1] - p[1:n-2] + q[2:n-1]*h^2) ./ (r[2:n-1]*h^2);
  ev = p[2:n-2] ./ (sqrt.(r[2:n-2].*r[3:n-1])*h^2);
  A = SymTridiagonal(dv, ev)


  return lfail_balloon, A
end


# A function that returns the stiffness (A) and mass (M) matrices and their
# derivatives for a given surface as a function of initial position (θ, ζ).
# This function is used as the input to eig_opt.
function getmatrix_function(xmin::Float64, h::Float64, n::Int64,
                            bci::BallooningCoefInterp)
   function AMfun(r)
      ι = get_iota(bci);
      θ = r[1];
      ζ = r[2];
      α = θ - ι*ζ;

      # Get (α, ζ) derivatives of the outputs of bci (c2, κ, bsuppar)
      ddcoeffs_f = ddcoeffs_struct(n)
      ddcoeffs_f.x .= xmin .+ [0:n-1;]*h;
      coeffs_derivs!(bci, ζ, α, ddcoeffs_f; eval_p=false);

      ddcoeffs_h = ddcoeffs_struct(n-1);
      ddcoeffs_h.x .= ddcoeffs_f.x[2:n] .- h/2;
      coeffs_derivs!(bci, ζ, α, ddcoeffs_h; eval_q=false, eval_r=false);

      p = ddcoeffs_h.p;
      q = ddcoeffs_f.q;
      r = ddcoeffs_f.r;

      # Form the matrices and their derivatives in the (α, ζ) frame
      A = SymTridiagonal(-p[2:n-1] - p[1:n-2] + q[2:n-1]*h^2, p[2:n-2])
      M = Diagonal(r[2:n-1] .* h^2)
      if !isposdef(M)
         if isposdef(-M)
            M = -M;
            A = -A;
         else
            println("AMfun: M is neither positive nor negative definite");
            println("sum(diag(M) .< 0) = $(sum(diag(M) .< 0))")
         end
      end

      dA = Vector{SymTridiagonal}(undef, 2)
      dM = Vector{Diagonal}(undef, 2)
      dp = ddcoeffs_h.dp
      dq = ddcoeffs_f.dq
      dr = ddcoeffs_f.dr
      for ii = 1:2
         dA[ii] = SymTridiagonal(-dp[2:n-1, ii] - dp[1:n-2, ii] + dq[2:n-1, ii]*h^2, dp[2:n-2, ii])
         dM[ii] = Diagonal(dr[2:n-1,ii] .* h^2)
      end

      ddA = Matrix{SymTridiagonal}(undef, 2, 2)
      ddM = Matrix{Diagonal}(undef, 2, 2)
      ddp = ddcoeffs_h.ddp
      ddq = ddcoeffs_f.ddq
      ddr = ddcoeffs_f.ddr
      for ii = 1:2, jj = 1:2
         ddA[ii, jj] = SymTridiagonal(-ddp[2:n-1, ii, jj] - ddp[1:n-2, ii, jj] + ddq[2:n-1, ii, jj]*h^2, ddp[2:n-2, ii, jj])
         ddM[ii, jj] = Diagonal(ddr[2:n-1, ii, jj] .* h^2)
      end

      # Find matrix derivatives in the (θ, ζ) frame
      dA[2] = dA[2] - ι*dA[1];
      dM[2] = dM[2] - ι*dM[1];

      ddA[2, 2] = ddA[2, 2] - 2ι * ddA[1,2] + ι^2 * ddA[1,1];
      ddA[1, 2] = ddA[1, 2] - ι * ddA[1,1];
      ddA[2, 1] = ddA[1, 2];

      ddM[2, 2] = ddM[2, 2] - 2ι * ddM[1,2] + ι^2 * ddM[1,1];
      ddM[1, 2] = ddM[1, 2] - ι * ddM[1,1];
      ddM[2, 1] = ddM[1, 2];

      return A, dA, ddA, M, dM, ddM
   end

   return AMfun;
end
