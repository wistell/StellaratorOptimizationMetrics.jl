function get_nfp(bci::BallooningCoefInterp);  return bci.nfp; end
function get_iota(bci::BallooningCoefInterp); return bci.iota; end

## Helper functions for evaluating Fourier, Chebyshev things
function FourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = cos.(θ * (0:Nθ)');
  A[:, 2:2:end] = sin.(θ * (1:Nθ)');

  return A
end

function dFourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = - sin.(θ * (0:Nθ)')*Diagonal(0:Nθ);
  A[:, 2:2:end] = cos.(θ * (1:Nθ)')*Diagonal(1:Nθ);

  return A
end

function ddFourierMat(θ::AbstractVector, Nθ::Integer)
  A = zeros(length(θ), 2Nθ+1);
  A[:, 1:2:end] = - cos.(θ * (0:Nθ)')*Diagonal((0:Nθ).^2)
  A[:, 2:2:end] = - sin.(θ * (1:Nθ)')*Diagonal((1:Nθ).^2)

  return A
end


function FourierGridMat(Nθ::Integer; backward::Bool=false)
  θ = (0:2Nθ+1) .* (2π/(2Nθ+2))
  A = FourierMat(θ, Nθ);

  if backward
    for n = 1:2Nθ+1
      A[:, n] = A[:,n] ./ (A[:,n]'*A[:,n])
    end
  end

  return A;
end

function ChebyshevMat(θ::AbstractVector, Nζ::Integer)
  return cos.(θ * (0:Nζ)');
end

function dChebyshevMat(θ::AbstractVector, Nζ::Integer)
  A = zeros(length(θ), Nζ+1);
  i_0 = (θ .< 1e-8)
  i_π = (θ .> π-1e-8)
  i_e = (.!i_0) .&& (.!i_π)

  n = 0:Nζ
  A[i_0, :] .= ones(sum(i_0)) * (n.^2)'
  A[i_π, :] .= ones(sum(i_π)) * (-(-1).^n .* n.^2)'
  A[i_e, :] .= Diagonal(csc.(θ[i_e])) * sin.(θ[i_e] * n') * Diagonal(n);

  return A
end

function ddChebyshevMat(θ::AbstractVector, Nζ::Integer)
  A = zeros(length(θ), Nζ+1);

  i_0 = (θ .< 1e-8)
  i_π = (θ .> π-1e-8)
  i_e = (.!i_0) .&& (.!i_π)

  n = 0:Nζ

  v = -(1/3) .* n.^2 .* (1 .- n.^2);
  A[i_0, :] .= ones(sum(i_0)) * v'

  v =  (-1).^n .* v
  A[i_π, :] .= ones(sum(i_π)) * v'

  cscθ = csc.(θ[i_e]);
  A[i_e, :] .= -Diagonal(cscθ.^2) * cos.(θ[i_e] * n') * Diagonal(n.^2)
  A[i_e, :] .= A[i_e, :] + Diagonal(cscθ.^3 .* cos.(θ[i_e])) * sin.(θ[i_e] * n') * Diagonal(n);

  return A;
end


function ChebyshevGridMat(Nζ::Integer; backward::Bool=false)
  A = ChebyshevMat(LinRange(0, π, Nζ+1), Nζ)

  if backward
    # Quadrature rule
    w = ones(Nζ+1)
    w[1] = 1/2; w[end] = 1/2;
    W = Diagonal(w);

    for n = 1:Nζ+1
      A[:, n] = W*A[:,n] ./ (A[:,n]'*W*A[:,n])
    end
  end

  return A;
end


## Include implementations

include("./FCInterp.jl")  # Fourier-Chebyshev interpolation
include("./SInterp.jl")   # Spline interpolation


function eval_coefs(bci::BallooningCoefInterp, α::Number, ζ::AbstractVector;
                    eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  c2_nodes, κ_nodes, bsuppar_nodes = eval_coefs(bci, [α], ζ; eval_c2, eval_κ, eval_bsuppar)
  nζ = length(ζ);
  return reshape(c2_nodes, nζ, 3), reshape(κ_nodes, nζ, 2), vec(bsuppar_nodes)
end




function ddeval_coefs(bci::BallooningCoefInterp, α::Number, ζ::AbstractVector;
                      eval_c2::Bool=true, eval_κ::Bool=true, eval_bsuppar::Bool=true)
  c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar = (
                       ddeval_coefs(bci, [α], ζ; eval_c2, eval_κ, eval_bsuppar))
  nζ = length(ζ);

  if eval_c2
    c2 = reshape(c2, nζ, 3)
    dc2 = reshape(dc2, nζ, 2, 3)
    ddc2 = reshape(ddc2, nζ, 2, 2, 3)
  end

  if eval_κ
    κ = reshape(κ, nζ, 2)
    dκ = reshape(dκ, nζ, 2, 2)
    ddκ = reshape(ddκ, nζ, 2, 2, 2)
  end

  if eval_bsuppar
    bsuppar = reshape(bsuppar, nζ)
    dbsuppar = reshape(dbsuppar, nζ, 2)
    ddbsuppar = reshape(ddbsuppar, nζ, 2, 2)
  end

  return c2, κ, bsuppar, dc2, dκ, dbsuppar, ddc2, ddκ, ddbsuppar
end

# Map alpha forward a sheet
#  (α, ζ) -> (map_α(bci, α), ζ-2π/nfp)
function map_α(bci::BallooningCoefInterp, α::AbstractVector)
  return α .+ (2π * get_iota(bci) / get_nfp(bci));
end

function map_α(bci::BallooningCoefInterp, α::Number)
  return map_α(bci, [α])[1]
end

# Map alpha backwards a sheet
#  (α, ζ) -> (map_α_inv(bci, α), ζ+2π/nfp)
function map_α_inv(bci::BallooningCoefInterp, α::AbstractVector)
  return α .- (2π * get_iota(bci) / get_nfp(bci));
end

function map_α_inv(bci::BallooningCoefInterp, α::Number)
  return map_α_inv(bci, [α])[1]
end


## Finally, constructors
function BallooningCoefInterp(surf::S, Nζ::Integer,
                              Nα::Integer; c2_nodes=[], κ_nodes = [],
                              bsuppar_nodes = [], oversample_factor=1.5,
                              spline::Bool=false
                             ) where {S <: AbstractSurface}
  if spline
    return SInterp(surf, Nζ, Nα; c2_nodes, κ_nodes, bsuppar_nodes)
  end

  FCInterp(surf, Nζ, Nα; c2_nodes, κ_nodes, bsuppar_nodes)
end

function BallooningCoefInterp(surf::S, Nζ::Integer,
                              Nα::Integer, params::BalloonParams;
                              spline::Bool=false
                             ) where {S <: AbstractSurface}

  bci = spline ? SInterp(surf, Nζ, Nα) : FCInterp(surf, Nζ, Nα)

  scale_bci(bci, surf, params)
  return bci
end


function get_err_on_grid(bci, c2_nodes, κ_nodes, bsuppar_nodes, α, ζ)
  c2_α_interp, κ_α_interp, bsuppar_α_interp = eval_coefs(bci, α[2:2:end], ζ[1:2:end])

  c2_α_err = c2_nodes[2:2:end, 1:2:end,:] - c2_α_interp
  κ_α_err = κ_nodes[2:2:end, 1:2:end,:] - κ_α_interp
  bsuppar_α_err = bsuppar_nodes[2:2:end, 1:2:end] - bsuppar_α_interp

  c2_α_relerr = norm(c2_α_err)/norm(c2_nodes[2:2:end, 1:2:end,:])
  κ_α_relerr = norm(κ_α_err)/norm(κ_nodes[2:2:end, 1:2:end,:])
  bsuppar_α_relerr = norm(bsuppar_α_err)/norm(bsuppar_nodes[2:2:end, 1:2:end])

  c2_ζ_interp, κ_ζ_interp, bsuppar_ζ_interp = eval_coefs(bci, α[1:2:end], ζ[2:2:end])

  c2_ζ_err = c2_nodes[1:2:end, 2:2:end,:] - c2_ζ_interp
  κ_ζ_err = κ_nodes[1:2:end, 2:2:end,:] - κ_ζ_interp
  bsuppar_ζ_err = bsuppar_nodes[1:2:end, 2:2:end] - bsuppar_ζ_interp

  c2_ζ_relerr = norm(c2_ζ_err)/norm(c2_nodes[1:2:end, 2:2:end, :])
  κ_ζ_relerr = norm(κ_ζ_err)/norm(κ_nodes[1:2:end, 2:2:end, :])
  bsuppar_ζ_relerr = norm(bsuppar_ζ_err)/norm(bsuppar_nodes[1:2:end, 2:2:end])

  return [c2_α_relerr, κ_α_relerr, bsuppar_α_relerr], [c2_ζ_relerr, κ_ζ_relerr, bsuppar_ζ_relerr]
end

## Adaptive interpolation
#   Reuses previously computed grid points, so the number of evaluations of
#   field lines is kept to a minimum. Refines in α and ζ differently, so
#   it is possible to have a lower-order interpolant in α or ζ
function BallooningCoefInterp(surf::S;
                              reltol::Number=1e-4, spline::Bool=false
                             ) where {S <: AbstractSurface}

  errF = 1;
  errC = 1;
  Nα, Nζ = spline ? S_refinement_init() : FC_refinement_init();
  Nrefinement=4

  nfp = surf.nfp;
  iota = surf.iota[1];

  αvec, ζs  = spline ? S_refinement_grid(Nα, Nζ, nfp) : FC_refinement_grid(Nα, Nζ, nfp)
  # Get interpolant on initial grid
  c2_nodes, κ_nodes, bsuppar_nodes = ballooning_coefficient_grid(surf, ζs, αvec)
  bci = BallooningCoefInterp(surf, Nζ, Nα; c2_nodes, κ_nodes, bsuppar_nodes, spline)


  αdoubled = true; ζdoubled = true;
  for ii = 1:Nrefinement
    # Refine the grid
    Nα_next, Nζ_next = spline ? S_refinement_increment(Nα, Nζ) : FC_refinement_increment(Nα, Nζ)
    α, ζ = spline ? S_refinement_grid(Nα_next, Nζ_next, nfp) : FC_refinement_grid(Nα_next, Nζ_next, nfp)

    Mα = length(α); Mζ = length(ζ)

    c2_nodes_next = zeros(Mα, Mζ, 3);
    κ_nodes_next = zeros(Mα, Mζ, 2);
    bsuppar_nodes_next = zeros(Mα, Mζ);

    # Fill refined grid with already known coefficients
    if αdoubled && ζdoubled
      c2_nodes_next[1:2:end, 1:2:end, :] = c2_nodes;
      κ_nodes_next[1:2:end, 1:2:end, :] = κ_nodes;
      bsuppar_nodes_next[1:2:end, 1:2:end] = bsuppar_nodes;
    elseif αdoubled
      c2_nodes_next[1:2:end, :, :] = c2_nodes;
      κ_nodes_next[1:2:end, :, :] = κ_nodes;
      bsuppar_nodes_next[1:2:end, :, :] = bsuppar_nodes;
    else
      c2_nodes_next[:, 1:2:end, :] = c2_nodes;
      κ_nodes_next[:, 1:2:end, :] = κ_nodes;
      bsuppar_nodes_next[:, 1:2:end, :] = bsuppar_nodes;
    end
    c2_nodes, κ_nodes, bsuppar_nodes = c2_nodes_next, κ_nodes_next, bsuppar_nodes_next;

    # Compute needed coefficients to calculate error
    if αdoubled
      c2_α, κ_α, bsuppar_α = ballooning_coefficient_grid(surf, ζ[1:2:end], α[2:2:end])
      c2_nodes[2:2:end, 1:2:end, :] = c2_α;
      κ_nodes[2:2:end, 1:2:end, :] = κ_α;
      bsuppar_nodes[2:2:end, 1:2:end] = bsuppar_α;
    end

    if ζdoubled
      c2_ζ, κ_ζ, bsuppar_ζ = ballooning_coefficient_grid(surf, ζ[2:2:end], α[1:2:end])
      c2_nodes[1:2:end, 2:2:end, :] = c2_ζ;
      κ_nodes[1:2:end, 2:2:end, :] = κ_ζ;
      bsuppar_nodes[1:2:end, 2:2:end] = bsuppar_ζ;
    end

    # Evaluate error in α and ζ directions
    αerrs, ζerrs = get_err_on_grid(bci, c2_nodes, κ_nodes, bsuppar_nodes, α, ζ)

    # Check error to see if refinement is necessary in both directions
    αdoubled = maximum(αerrs) > reltol
    if αdoubled
      Nα = Nα_next;
    end

    ζdoubled = maximum(ζerrs) > reltol
    if ζdoubled
      Nζ = Nζ_next
    end

    if !αdoubled && !ζdoubled
      return bci
    end

    # Get the rest of the values on the grid
    c2_αζ, κ_αζ, bsuppar_αζ = ballooning_coefficient_grid(surf, ζ[2:2:end], α[2:2:end]);
    c2_nodes[2:2:end, 2:2:end, :] = c2_αζ;
    κ_nodes[2:2:end, 2:2:end, :] = κ_αζ;
    bsuppar_nodes[2:2:end, 2:2:end] = bsuppar_αζ;

    #
    if αdoubled && ζdoubled
      bci = BallooningCoefInterp(surf, Nζ, Nα; c2_nodes, κ_nodes,
                                 bsuppar_nodes, spline)
    elseif αdoubled
      bci = BallooningCoefInterp(surf, Nζ, Nα; c2_nodes=c2_nodes[:, 1:2:end, :],
                                 κ_nodes=κ_nodes[:, 1:2:end, :],
                                 bsuppar_nodes=bsuppar_nodes[:, 1:2:end], spline)
    else
      bci = BallooningCoefInterp(surf, Nζ, Nα; c2_nodes=c2_nodes[1:2:end, :, :],
                                 κ_nodes=κ_nodes[1:2:end, :, :],
                                 bsuppar_nodes=bsuppar_nodes[1:2:end, :], spline)
    end
  end

  return bci
end
#
function BallooningCoefInterp(surf::S, params::BalloonParams;
                              reltol::Number=1e-4, spline=false
                              ) where {S <: AbstractSurface}
  bci = BallooningCoefInterp(surf; reltol, spline)
  scale_bci(bci, surf, params)
  return bci
end
