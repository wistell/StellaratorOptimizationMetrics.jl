module StellaratorOptimizationMetrics
using Requires
using LinearAlgebra
using StaticArrays
using SparseArrays
using Interpolations
using Polyester
using Roots
using QuadGK
using DoubleExponentialFormulas
using ArnoldiMethod

using PlasmaEquilibriumToolkit

const StellOptMetrics = StellaratorOptimizationMetrics
export StellOptMetrics
export gamma_c_target
export cobra, cobra_opt
export ∇B_double_dot_∇B_target, L∇B_target

include("GammaC/GammaC.jl")
include("BallooningStability/BallooningStability.jl")
include("FieldMetrics/FieldMetrics.jl")

end
