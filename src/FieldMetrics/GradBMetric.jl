# See the note "20181220-05 Computing grad B tensor from VMEC.docx" for
# some of the formulae in this code.

# Also see "20190125-01 2nd pass computing grad B tensor from vmec.docx"
# and m20181220_03_BExponentiationMeasure.m

# Adapted to julia by Aaron Bader from a matlab code by Matt Landreman

function spline_hessian(spl::Interpolations.Extrapolation, 
                 θs::StepRangeLen, 
                 ζs::StepRangeLen)

  Nθ = length(θs)
  Nζ = length(ζs)
  hess = [Interpolations.hessian(spl, θ, ζ) for θ in θs, ζ in ζs]
  dx2 = [hess[iθ,iζ][1,1] for iθ in 1:Nθ, iζ in 1:Nζ] 
  dxdy = [hess[iθ,iζ][1,2] for iθ in 1:Nθ, iζ in 1:Nζ] 
  dy2 = [hess[iθ,iζ][2,2] for iθ in 1:Nθ, iζ in 1:Nζ] 
  
  return dx2, dxdy, dy2
end

"""
  `∇B_double_dot_∇B(surf, Nθ, Nζ)`

calculate the value of ∇B:∇B on a given surface. 

# Arguments
 - `surf::Surface`:A Vmec or DESC Surface object
 - `Nθ::Int`: Resolution in the poloidal direction (will recalculate splines if necessary)
 - `Nζ::Int`: Resolution in the toroidal direction (will recalculate splines if necessary)

"""
function ∇B_double_dot_∇B(surf::ST, Nθ::Int, Nζ::Int) where {ST <: AbstractMagneticSurface}


  nfp = surf.nfp

  #because of issues extrapolating the Hessian we can't use the last value
  delθ = 2π/(Nθ-1)
  delζ = 2π/nfp/(Nζ-1)
  θs = range(0,stop=2*pi-delθ,length=Nθ);
  ζs = range(0,stop=2*pi/nfp-delζ,length=Nζ);

  Nθ > 128 ? minθ = Nθ : minθ = 128
  Nζ > 128 ? minζ = Nζ : minζ = 128
  

  #[zeta2D, theta2D] = meshgrid(zeta,theta);

  R = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :r,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  Z = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :z,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]

  dRds = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :drds, 
                      minθres = minθ, minζres = minζ) 
                for θ in θs, ζ in ζs]
  dRdθ = [surface_get_exact(FluxCoordinates(0.0, θ, ζ), surf, :rmn, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  dRdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :r, deriv = :dζ) 
                for θ in θs, ζ in ζs]

  dZds = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dzds, 
                      minθres = minθ, minζres = minζ) 
                for θ in θs, ζ in ζs]
  dZdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :z, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  dZdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :z, deriv = :dζ) 
                for θ in θs, ζ in ζs]

  d2Rdsdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :drds, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  d2Rdsdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :drds, deriv = :dζ) 
                for θ in θs, ζ in ζs]
  d2Rdθ2, d2Rdθdζ, d2Rdζ2 = spline_hessian(surf.r, θs, ζs)

  d2Zdsdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dzds, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  d2Zdsdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dzds, deriv = :dζ) 
                for θ in θs, ζ in ζs]
  d2Zdθ2, d2Zdθdζ, d2Zdζ2 = spline_hessian(surf.z, θs, ζs)

  B = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :b,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]


  dBds = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dbds,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  dBdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :b, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  dBdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :b, deriv = :dζ) 
                for θ in θs, ζ in ζs]

  sqrt_g = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :g,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]

  Bsupθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupu,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  dBsupθds = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dbsupuds,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  dBsupθdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupu, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  dBsupθdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupu, deriv = :dζ) 
                for θ in θs, ζ in ζs]

  Bsupζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupv,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  dBsupζds = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :dbsupvds,
                      minθres = minθ, minζres = minζ) 
                   for θ in θs, ζ in ζs]
  dBsupζdθ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupv, deriv = :dθ) 
                for θ in θs, ζ in ζs]
  dBsupζdζ = [surface_get(FluxCoordinates(0.0, θ, ζ), surf, :bsupv, deriv = :dζ) 
                for θ in θs, ζ in ζs]


  cosζ = Array{Float64}(undef,(Nθ,Nζ))
  sinζ = Array{Float64}(undef,(Nθ,Nζ))

  for i in 1:Nθ
    cosζ[i,:] = cos.(ζs)
    sinζ[i,:] = sin.(ζs)
  end
  #cosζ = cos.(ζs);
  #sinζ = sin.(ζs);

  ∇s__R = - dZdθ .* R ./ sqrt_g;
  ∇s__ζ = (dRdζ .* dZdθ - dRdθ .* dZdζ) ./ sqrt_g;
  ∇s__Z = dRdθ .* R ./ sqrt_g;
  ∇s__X = ∇s__R .* cosζ + ∇s__ζ .* (-sinζ) 
  ∇s__Y = ∇s__R .* sinζ + ∇s__ζ .* cosζ

  ∇θ__R = dZds .* R ./ sqrt_g;
  ∇θ__ζ = (dRds .* dZdζ - dRdζ .* dZds) ./ sqrt_g;
  ∇θ__Z = -dRds .* R ./ sqrt_g;
  ∇θ__X = ∇θ__R .* cosζ + ∇θ__ζ .* (-sinζ) 
  ∇θ__Y = ∇θ__R .* sinζ + ∇θ__ζ .* cosζ 

  ∇ζ__R = 0 * sqrt_g;
  ∇ζ__ζ = 1 ./ R;
  ∇ζ__ζ_alt = (dRdθ .* dZds - dRds .* dZdθ) ./ sqrt_g;
  ∇ζ__Z = 0 * sqrt_g;
  ∇ζ__X = ∇ζ__R .* cosζ + ∇ζ__ζ .* (-sinζ) 
  ∇ζ__Y = ∇ζ__R .* sinζ + ∇ζ__ζ .* cosζ 

  @debug println("Should be close to 0: ",maximum(abs.(∇ζ__ζ - ∇ζ__ζ_alt)))

  ∇B__X = dBds .* ∇s__X + dBdθ .* ∇θ__X + dBdζ .* ∇ζ__X;
  ∇B__Y = dBds .* ∇s__Y + dBdθ .* ∇θ__Y + dBdζ .* ∇ζ__Y;
  ∇B__Z = dBds .* ∇s__Z + dBdθ .* ∇θ__Z + dBdζ .* ∇ζ__Z;

  norm_∇B = sqrt.(∇B__X .^ 2 + ∇B__Y .^ 2 + ∇B__Z .^ 2);

  norm_∇ln_B = norm_∇B ./ B;






  dBx_ds = (dBsupθds .* dRdθ .* cosζ 
      + Bsupθ .* d2Rdsdθ .* cosζ 
      + dBsupζds .* dRdζ .* cosζ 
      + Bsupζ .* d2Rdsdζ .* cosζ 
      - dBsupζds .* R .* sinζ 
      - Bsupζ .* dRds .* sinζ);

  dBx_dθ = (dBsupθdθ .* dRdθ .* cosζ 
      + Bsupθ .* d2Rdθ2 .* cosζ 
      + dBsupζdθ .* dRdζ .* cosζ 
      + Bsupζ .* d2Rdθdζ .* cosζ 
      - dBsupζdθ .* R .* sinζ 
      - Bsupζ .* dRdθ .* sinζ);

  dBx_dζ = (dBsupθdζ .* dRdθ .* cosζ 
      + Bsupθ .* d2Rdθdζ .* cosζ 
      - Bsupθ .* dRdθ .* sinζ 
      + dBsupζdζ .* dRdζ .* cosζ 
      + Bsupζ .* d2Rdζ2 .* cosζ 
      - Bsupζ .* dRdζ .* sinζ 
      - dBsupζdζ .* R .* sinζ 
      - Bsupζ .* dRdζ .* sinζ 
      - Bsupζ .* R .* cosζ);

  dBy_ds = (dBsupθds .* dRdθ .* sinζ 
      + Bsupθ .* d2Rdsdθ .* sinζ 
      + dBsupζds .* dRdζ .* sinζ 
      + Bsupζ .* d2Rdsdζ .* sinζ 
      + dBsupζds .* R .* cosζ 
      + Bsupζ .* dRds .* cosζ);

  dBy_dθ = (dBsupθdθ .* dRdθ .* sinζ 
      + Bsupθ .* d2Rdθ2 .* sinζ 
      + dBsupζdθ .* dRdζ .* sinζ 
      + Bsupζ .* d2Rdθdζ .* sinζ 
      + dBsupζdθ .* R .* cosζ 
      + Bsupζ .* dRdθ .* cosζ);

  dBy_dζ = (dBsupθdζ .* dRdθ .* sinζ 
      + Bsupθ .* d2Rdθdζ .* sinζ 
      + Bsupθ .* dRdθ .* cosζ 
      + dBsupζdζ .* dRdζ .* sinζ 
      + Bsupζ .* d2Rdζ2 .* sinζ 
      + Bsupζ .* dRdζ .* cosζ 
      + dBsupζdζ .* R .* cosζ 
      + Bsupζ .* dRdζ .* cosζ 
      - Bsupζ .* R .* sinζ);

  dBz_ds = (dBsupθds .* dZdθ 
      + Bsupθ .* d2Zdsdθ 
      + dBsupζds .* dZdζ 
      + Bsupζ .* d2Zdsdζ);

  dBz_dθ = (dBsupθdθ .* dZdθ 
      + Bsupθ .* d2Zdθ2 
      + dBsupζdθ .* dZdζ 
      + Bsupζ .* d2Zdθdζ);

  dBz_dζ = (dBsupθdζ .* dZdθ 
      + Bsupθ .* d2Zdθdζ 
      + dBsupζdζ .* dZdζ 
      + Bsupζ .* d2Zdζ2);

  ∇B__XX = dBx_ds .* ∇s__X + dBx_dθ .* ∇θ__X + dBx_dζ .* ∇ζ__X;
  ∇B__XY = dBx_ds .* ∇s__Y + dBx_dθ .* ∇θ__Y + dBx_dζ .* ∇ζ__Y;
  ∇B__XZ = dBx_ds .* ∇s__Z + dBx_dθ .* ∇θ__Z + dBx_dζ .* ∇ζ__Z;

  ∇B__YX = dBy_ds .* ∇s__X + dBy_dθ .* ∇θ__X + dBy_dζ .* ∇ζ__X;
  ∇B__YY = dBy_ds .* ∇s__Y + dBy_dθ .* ∇θ__Y + dBy_dζ .* ∇ζ__Y;
  ∇B__YZ = dBy_ds .* ∇s__Z + dBy_dθ .* ∇θ__Z + dBy_dζ .* ∇ζ__Z;

  ∇B__ZX = dBz_ds .* ∇s__X + dBz_dθ .* ∇θ__X + dBz_dζ .* ∇ζ__X;
  ∇B__ZY = dBz_ds .* ∇s__Y + dBz_dθ .* ∇θ__Y + dBz_dζ .* ∇ζ__Y;
  ∇B__ZZ = dBz_ds .* ∇s__Z + dBz_dθ .* ∇θ__Z + dBz_dζ .* ∇ζ__Z;

  div_B = ∇B__XX + ∇B__YY + ∇B__ZZ;

  ∇B_double_dot_∇B_val = (  
        ∇B__XX .* ∇B__XX 
      + ∇B__XY .* ∇B__XY 
      + ∇B__XZ .* ∇B__XZ 
      + ∇B__YX .* ∇B__YX 
      + ∇B__YY .* ∇B__YY 
      + ∇B__YZ .* ∇B__YZ 
      + ∇B__ZX .* ∇B__ZX 
      + ∇B__ZY .* ∇B__ZY 
      + ∇B__ZZ .* ∇B__ZZ);


#  norm_∇s = sqrt.(∇s__X .^ 2 + ∇s__Y .^ 2 + ∇s__Z .^2);
#  normal__X = ∇s__X ./ norm_∇s;
#  normal__Y = ∇s__Y ./ norm_∇s;
#  normal__Z = ∇s__Z ./ norm_∇s;
      
#  distance_to_singularity = 4 * B .* norm_∇B ./ ∇B_double_dot_∇B_val;

#  new_directional_figure_of_merit = 4 * B .* (∇B__X .* normal__X + ∇B__Y .* normal__Y + ∇B__Z .* normal__Z) ./ ∇B_double_dot_∇B_val;

#  temp = normal__X .^ 2 + normal__Y .^ 2 + normal__Z .^ 2;


#  p1 = contour(ζ,theta,∇B_double_dot_∇B)
#  plot(p1)
#  gui()
  #return gradBlength_met, ζs, θs
  return ∇B_double_dot_∇B_val, B, ζs, θs
  end

"""
  `∇B_double_dot_∇B_target(surf, Nθ, Nζ)`

calculate the maximum value of ∇B:∇B on a given surface. 

# Arguments
 - `surf::Surface`:A Vmec or DESC object
 - `Nθ::Int`: Resolution in the poloidal direction (will recalculate splines if necessary)
 - `Nζ::Int`: Resolution in the toroidal direction (will recalculate splines if necessary)

"""
function ∇B_double_dot_∇B_target(surf::ST, Nθ::Int, Nζ::Int) where {ST <: AbstractMagneticSurface}
  gradBval, ~, ~ = ∇B_double_dot_∇B(surf, Nθ, Nζ)
  return maximum(gradBval)
end

function L∇B(surf::ST, Nθ::Int, Nζ::Int) where {ST <: AbstractMagneticSurface}
  gradBval, B, ~, ~ = ∇B_double_dot_∇B(surf, Nθ, Nζ)
  return sqrt(2) * B ./sqrt.(gradBval);
end

function min_L∇B(surf::ST, Nθ::Int, Nζ::Int) where {ST <: AbstractMagneticSurface}
  return minimum(L∇B(surf, Nθ, Nζ))
end

function L∇B_target(surf::ST, Nθ::Int, Nζ::Int) where {ST <: AbstractMagneticSurface}
  #normalize
  norm_L∇B = min_L∇B(surf, Nθ, Nζ) / surf.Aminor_p
  #to get a function to minimize, we need the reciprocal
  return 1.0/norm_L∇B
end
