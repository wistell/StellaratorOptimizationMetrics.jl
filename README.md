# StellaratorOptimizationMetrics

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://wistell.gitlab.io/StellaratorOptimizationMetrics.jl/dev)
[![Build Status](https://gitlab.com/WISTELL/StellaratorOptimizationMetrics.jl/badges/master/pipeline.svg)](https://gitlab.com/WISTELL/StellaratorOptimizationMetrics.jl/pipelines)
[![Coverage](https://gitlab.com/WISTELL/StellaratorOptimizationMetrics.jl/badges/master/coverage.svg)](https://gitlab.com/WISTELL/StellaratorOptimizationMetrics.jl/commits/master)

Collection of metrics and reduced models for use in stellarator optimization calculations.
    - Quasisymmetry
    - Neoclassical transport: ``\epsilon_{eff}``
    - Fast particle transport: ``\Gamma_c``
    - MHD Ballooning Stability: inspired by the [COBRAVMEC package](https://princetonuniversit.github.io/STELLOPT/COBRAVMEC.html)
    - ``\nabla B : \nabla B``
